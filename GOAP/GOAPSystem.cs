﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace IPCA.AI.GOAP
{
	/// <summary>
	/// Goal Oriented Action Planning System
	/// </summary>
	public class GOAPSystem
	{
		List<Goal> goals;
		List<GOAPAction> actions;


		/// <summary>
		/// Empty constructor. Goals and actions need to be added manually.
		/// </summary>
		public GOAPSystem() {
			goals = new List<Goal>();
			actions = new List<GOAPAction>();
		}

		/// <summary>
		/// Constructor receiving list of goals and actions.
		/// </summary>
		/// <param name="goals">Goals to be added to the system.</param>
		/// <param name="actions">Actions to be added to the system</param>
		public GOAPSystem(IEnumerable<Goal> goals, IEnumerable<GOAPAction> actions) : this()
		{
			AddGoals(goals.ToArray());
			AddActions(actions.ToArray());
			this.actions = new List<GOAPAction>(actions.ToArray());
		}

		public void AddActions(params GOAPAction[] actions)
		{
			this.actions.AddRange(actions);
		}

		public void AddGoals(params Goal[] goals)
		{
			this.goals.AddRange(goals);
		}

		/// <summary>
		/// Executes an action.
		/// </summary>
		/// <param name="action">Name of the action to be executed</param>
		public void ExecuteAction(string action)
		{
			// FIXME: Make actions have a Action code reference
			Console.WriteLine("Executing action: " + action);
			// FIXME: need to update goals accordingly with action
		}

		/// <summary>
		/// Chooses the best action o be executed.
		/// </summary>
		/// <returns>The chosen action</returns>
		public GOAPAction ChooseAction()
		{
			// percorrer cada ação e calcular descontentamento
			GOAPAction bestAction = actions.First();
			float bestValue = CalculateDiscontentment(bestAction, goals);
			foreach (GOAPAction action in actions.Skip(1))
			{
				float thisValue = CalculateDiscontentment(action, goals);
				if (thisValue < bestValue)
				{
					bestValue = thisValue;
					bestAction = action;
				}
			}

			return bestAction;
		}

		private float CalculateDiscontentment(GOAPAction action, IEnumerable<Goal> goals)
		{
			// inicializar acumulador
			float discontentment = 0f;
			// percorrer todos os objetivos
			foreach (Goal goal in goals)
			{
				// calcular insistencia apos acao
				float newValue = goal.Value + action.GetGoalChange(goal.Name);
				// calcular alteração ao longo do tempo
				newValue += action.GetDuration() * goal.GetChange();
				// acumular descontentamento
				discontentment += newValue * newValue;
			}
			return discontentment;
		}


	}
}
