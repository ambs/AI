﻿using System;
namespace IPCA.AI.GOAP
{
	/// <summary>
	/// Simple Goal implementation, with name, value, and over-time change.
	/// </summary>
	public class Goal
	{
		/// <summary>
		/// Name of the Goal.
		/// </summary>
		/// <returns>String with the goal name.</returns>
		public string Name { get; private set; }
		/// <summary>
		/// Goal current value.
		/// </summary>
		/// <returns>Float representing current goal insistence.</returns>
		public float Value { get; private set; }

		float change;

		/// <summary>
		/// Goal constructor, without insistence or over-time change.
		/// </summary>
		/// <param name="n">Goal name</param>
		/// <returns>A new goal object</returns>
		public Goal(string n) : this(n, 0, 0) { }
		/// <summary>
		/// Goal constructor, given its name, current value, and over-time change.
		/// </summary>
		/// <param name="n">Name</param>
		/// <param name="v">Current insistence</param>
		/// <param name="inc">Over-time insistence change</param>
		public Goal(string n, float v, float inc)
		{
			Name = n;
			Value = v;
			change = inc;
		}

		/// <summary>
		/// Returns current over-time change.
		/// </summary>
		/// <returns>Float value with over-time insistence change.</returns>
		public float GetChange()
		{   // FIXME: Make this dynamic.
			return change;
		}
	}
}
