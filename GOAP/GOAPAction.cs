﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace IPCA.AI.GOAP
{
	/// <summary>
	/// A GOAP Action implementation
	/// </summary>
	public class GOAPAction
	{
		Dictionary<string, float> goalChanges;
		float duration;

		/// <summary>
		/// GOAP Action constructor.
		/// </summary>
		/// <param name="duration">The action duration in time units.</param>
		/// <param name="goalChanges">A list of pairs with goal names and value changes.</param>
		public GOAPAction(float duration,
		                  IEnumerable<Tuple<string, float>> goalChanges) : this(duration, goalChanges.ToArray()) {}
		
		/// <summary>
		/// GOAP Action constructor.
		/// </summary>
		/// <param name="duration">The action duration in time units.</param>
		/// <param name="goalChanges">A list of pairs with goal names and value changes.</param>
		public GOAPAction(float duration,
		                  params Tuple<string, float>[] goalChanges)
		{
			this.duration = duration;
			this.goalChanges = new Dictionary<string, float>();
			foreach (var gc in goalChanges)
			{
				this.goalChanges[gc.Item1] = gc.Item2;
			}
		}


		/// <summary>
		/// Gets how much a goal changes using this action
		/// </summary>
		/// <param name="goal">The name of the action to query</param>
		/// <returns>The amount the goal insistence changes when this action is performed</returns>
		public float GetGoalChange(string goal)
		{
			return goalChanges.ContainsKey(goal) ? goalChanges[goal] : 0f;
		}

		/// <summary>
		/// Gets action duration.
		/// </summary>
		/// <returns>Units of time action takes to complete.</returns>
		public float GetDuration() // FIXME: make this dynamic.
		{
			return duration;
		}

		/// <summary>
		/// ToString override, presenting action in readable way.
		/// </summary>
		/// <returns>Stringified Action</returns>
		public override string ToString()
		{
			string txt = "";
			txt = String.Join(",", goalChanges.Select(
				(arg1) => string.Format("{0}:{1}", arg1.Key, arg1.Value)));
			return string.Format("[GOAPAction takes {0} for {1}]", duration, txt);
		}
	}
}
