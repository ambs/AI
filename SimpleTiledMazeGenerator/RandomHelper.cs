using System;

namespace SimpleTiledMazeGenerator{
internal static class RandomHelper {
    internal static int Range(this Random rng, int low, int high) {
        return rng.Next(low, high + 1);
    }
} }