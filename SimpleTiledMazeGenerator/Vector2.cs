using System;

namespace SimpleTiledMazeGenerator{
internal class Vector2 {
    internal float x {private set; get;}
    internal float y {private set; get; }
    internal Vector2(float x, float y) {
        this.x = x;
        this.y = y;
    }
    internal static Vector2 X { get { return new Vector2(1,0); }}
    internal static Vector2 Y { get { return new Vector2(0,1); }}
    internal static Vector2 Zero { get { return new Vector2(0,0); }}
    internal static Vector2 One { get { return new Vector2(1,1); }}

    public static Vector2 operator * (Vector2 vector, float factor) {
        return new Vector2(vector.x * factor, vector.y * factor);
    }
    public static Vector2 operator + (Vector2 v1, Vector2 v2) {
        return new Vector2(v1.x + v2.x, v1.y + v2.y);
    }
    public static Vector2 operator - (Vector2 v1, Vector2 v2) {
        return new Vector2(v1.x - v2.x, v1.y - v2.y);
    }
    }
}