using System;
namespace SimpleTiledMazeGenerator {
public class MazeRoom
{
    static Random rng;
    public static bool RandomDivisions = false;
    private int xOrigin, yOrigin;
    private int height, width;
    private bool isLeaf;
    private int hPos, vPos;
    private MazeRoom nw, ne, se, sw;
    private int closedDoor; // 0 == N, 1 == E, 2 == S, 3 == O

    void BuildWalls(BitMatrix b, Vector2 mazeOrigin)
    {
        if (isLeaf) return; // Sala sem divisoes
        // Vertical, topo (N) == 0
        BuildVHoleWall(b, closedDoor != 0, mazeOrigin,
            new Vector2(xOrigin + hPos, yOrigin),
            new Vector2(xOrigin + hPos, yOrigin + vPos - 1));
        // Vertical, fundo (S) == 2
        BuildVHoleWall(b, closedDoor != 2, mazeOrigin,
            new Vector2(xOrigin + hPos, yOrigin + vPos + 1),
            new Vector2(xOrigin + hPos, yOrigin + height - 1));
        // Horizontal, esquerdo (O) = 3
        BuildHHoleWall(b, closedDoor != 3, mazeOrigin,
            new Vector2(xOrigin, yOrigin + vPos),
            new Vector2(xOrigin + hPos - 1, yOrigin + vPos));
        //Horizontal, direito (E) == 1
        BuildHHoleWall(b, closedDoor != 1, mazeOrigin,
            new Vector2(xOrigin + hPos + 1, yOrigin + vPos),
            new Vector2(xOrigin + width - 1, yOrigin + vPos));
        b[yOrigin + vPos, xOrigin + hPos] = true;

        nw.BuildWalls(b, mazeOrigin);
        ne.BuildWalls(b, mazeOrigin);
        se.BuildWalls(b, mazeOrigin);
        sw.BuildWalls(b, mazeOrigin);
    }
 
   void BuildVHoleWall(BitMatrix b, bool hasDoor, Vector2 origin, Vector2 p1, Vector2 p2)
    {
        if (hasDoor)
        {   // Criar duas paredes
            int doorPos = _computeDoor(p1.y, p2.y);
            BuildVWall(b, origin, p1, p1 + Vector2.Y * (doorPos - 1));
            BuildVWall(b, origin, p1 + (Vector2.Y * (doorPos + 1)), p2);
            
        }
        else // Sem porta
            BuildVWall(b, origin, p1, p2);
    }

    int _computeDoor(float min, float max) {
        if (max - min <= 1) return 0;

        int doorPos = rng.Range(0, (int)(max - min - 1));
        if (doorPos % 2 != 0) doorPos++;
        
        return doorPos;
    }


    void BuildHHoleWall(BitMatrix b, bool hasDoor, Vector2 origin, Vector2 p1, Vector2 p2)
    {
        if (hasDoor)
        {   // Criar duas paredes
            int doorPos = _computeDoor(p1.x, p2.x);
            BuildHWall(b,origin, p1, p1 + Vector2.X * (doorPos - 1));
            BuildHWall(b,origin, p1 + (Vector2.X * (doorPos + 1)), p2);
        }
        else // Sem porta
            BuildHWall(b,origin, p1, p2);
    }
    void BuildVWall(BitMatrix b, Vector2 mazeOrigin, Vector2 p1, Vector2 p2)
    {
        p1 += mazeOrigin;  p2 += mazeOrigin;
        for (int i = (int) p1.y; i <= (int) p2.y; i++)
            b[i,(int) p1.x] = true;
    }

    void BuildHWall(BitMatrix b, Vector2 mazeOrigin, Vector2 p1, Vector2 p2)
    {
        p1 += mazeOrigin; p2 += mazeOrigin;
        for (int i = (int) p1.x; i <= (int) p2.x; i++)        
            b[(int)p1.y, i ] = true;
    }

    public BitMatrix AsBitMatrix() {
        BitMatrix b = new BitMatrix(height, width);
     //   BuildFrame(b);
        BuildWalls(b, Vector2.Zero);
        return b;
    }
    private void BuildFrame(BitMatrix b) {
        for (int i = 0; i < b.ColumnCount; i++)
            b[0,i] = b[b.RowCount-1, i] = true;
        for (int i = 0; i < b.RowCount; i++) 
            b[i,0] = b[i, b.ColumnCount-1] = true;
    }



    public MazeRoom(int w, int h) : this(0,0,w,h) {}

    public MazeRoom(int oX, int oY, int w, int h)
    {
        rng = rng ?? new Random();
        xOrigin = oX; yOrigin = oY;
        width = w; height = h;
        if (w <= 2 || h <= 2) isLeaf = true;
        else
        {
            isLeaf = false;
            closedDoor = rng.Range(0, 3);
            hPos = MazeRoom.RandomDivisions ? rng.Range(1, w) : w / 2;
            if (hPos % 2 == 0) hPos++;

            vPos = MazeRoom.RandomDivisions ? rng.Range(1, h) : h / 2; 
            if (vPos % 2 == 0) vPos++;
            
            nw = new MazeRoom(oX, oY, hPos, vPos);
            ne = new MazeRoom(oX + hPos + 1, oY, w-hPos - 1, vPos);
            sw = new MazeRoom(oX, oY + vPos+1, hPos, h - vPos-1);
            se = new MazeRoom(oX+hPos+1, oY+vPos+1, w-hPos-1, h-vPos-1);
        }
    }
}
}

