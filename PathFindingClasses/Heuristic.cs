using Microsoft.Xna.Framework;
using System;
namespace PathFindingClasses
{
    

public class Heuristic
	{
		Point end;
		public Heuristic(Point endNode)
		{
			end = endNode;
		}

		public float GetEstimatedCost(Point startNode)
		{
		//	return (end.ToVector2() - startNode.ToVector2()).Length();

        return Math.Abs(end.X - startNode.X) + Math.Abs(end.Y - startNode.Y);
		}
	}

}