using System.Collections.Generic;
using Microsoft.Xna.Framework;
using System.Linq;

namespace PathFindingClasses {
    class LinqListManager<T> : IListManager<T> where T:INode {

        private List<T> l;

        public int Count { get { return l.Count; } } 

        public LinqListManager() {
            l = new List<T>();
        }

        public void Add(T node) {
            l.Add(node);
        }

        public T Min() {
            return l.OrderBy((n) => n.Cost).First();
        }

        public T Find(Point p)
        {
            return l.Find(o => o.Node == p);
        }

        public void Remove(T node) {
            l.Remove(node);
        }

        public bool Contains(Point p) {
            return l.Exists(obj => obj.Node == p);
        }
    }
}