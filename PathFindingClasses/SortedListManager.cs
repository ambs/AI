using System;
using System.Collections.Generic;
using System.Collections;
using Microsoft.Xna.Framework;
using System.Linq;

namespace PathFindingClasses {
    class SortedListManager<T> : IListManager<T> where T:INode {
        private SortedList l;
        public int Count { get { return l.Count; } } 
        public SortedListManager() {
            l = new SortedList();
        }
        public void Add(T node) {
            l.Add(node, node);
        }
        public T Min() {
            return (T) l.GetByIndex(0);
        }
        public T Find(Point p) {
            foreach (var x in l.Values) {
                if (((INode)x).Node == p) return (T)x;
            }
            return default(T);
        }
        public void Remove(T node) {           
            l.Remove(node);
        }
        public bool Contains(Point p) {
            foreach (var x in l.Values) {
                if (((INode)x).Node == p) return true;
            }
            return false;
        }
    }
}