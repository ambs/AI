﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SimpleTiledMazeGenerator;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Diagnostics;

namespace PathFindingClasses
{
	public class Game1 : Game
	{
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		
		int size = 4;

		BitMatrix maze;
		BitMatrix opened;
		BitMatrix closed;
		Texture2D brick;
		Point start = new Point(0,0);
		Point end = new Point(400,200);

		Task tDikstra;
		Task tAstar;

		public Game1()
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";
		}

		protected override void Initialize()
		{
			int w = end.X + 1;
			int h = end.Y + 1;
			MazeRoom room = new MazeRoom(w, h);
			maze = room.AsBitMatrix();
			
			opened = new BitMatrix(h, w);
			closed = new BitMatrix(h, w);

			graphics.IsFullScreen = false;
			graphics.PreferredBackBufferWidth = w * size;
			graphics.PreferredBackBufferHeight = h * size;
			graphics.ApplyChanges();

			brick = new Texture2D(graphics.GraphicsDevice,4,4);
			brick.SetData<Color>(new Color[] {Color.White, Color.White, Color.White, Color.White,
			                                Color.White, Color.Gray, Color.Gray, Color.White,
											Color.White, Color.Gray, Color.Gray, Color.White,
											Color.White, Color.White, Color.White, Color.White
											});

			base.Initialize();
			System.Console.WriteLine("Press A for Dijsktra with Sorted List");
		
			System.Console.WriteLine("Press D for Dijsktra with Standard List and Linq");
		}

		protected override void LoadContent()
		{
			spriteBatch = new SpriteBatch(GraphicsDevice);
			
			tDikstra = new Task( DijkstraLinq );
			tAstar = new Task( DijkstraSorted );
		
		}

		class NodeRecord : INode
		{
			public Point node;
			public Connection connection;
			public int costSofar;
			public float Cost { get { return costSofar; }}
			public Point Node { get { return node; }}
			public int CompareTo(object o) { 
				INode on = ((INode)o);
				
				if (Node == on.Node) return 0;

				if (Cost < on.Cost) return -1;
				if (Cost > on.Cost) return 1;

				if (Node.X != on.Node.X) return Node.X - on.Node.X;
				if (Node.Y != on.Node.Y) return Node.Y - on.Node.Y;

				throw new Exception("NOP!");
			}
		}

		public class Connection
		{
			public	Point from;
			public Point to;
			public int cost;
			public Connection(Point o, Point t)
			{
				from = o; to = t; cost = 1;
			}
		}

		void markOpened(Point p) { opened[p.Y, p.X] = true; }
		void markClosed(Point p) { closed[p.Y, p.X] = true; }
		bool isWalkable(int x, int y)
		{
			if (x < 0 || y < 0 || x > end.X || y > end.Y) return false;
			return !maze[y,x];
		}
		List<Connection> getConnections(Point node)
		{
			List<Connection> l = new List<Connection>();

			if (isWalkable(node.X - 1, node.Y))
				// path left
				l.Add(new Connection(node,node - new Point(1,0)));
			if (isWalkable(node.X + 1, node.Y))
				// path right
				l.Add(new Connection(node,node + new Point(1,0)));
			if (isWalkable(node.X, node.Y - 1))
				// path up
				l.Add(new Connection(node,node - new Point(0,1)));
			if (isWalkable(node.X, node.Y + 1))
				// pathDown
				l.Add(new Connection(node,node + new Point(0,1)));

			return l;
		}


		class AstarNodeRecord : INode
		{
			public Point node;
			public Connection connection;
			public float costSofar;
			public float estimatedTotalCost;
			public float Cost { get { return estimatedTotalCost; }}
			public Point Node { get { return node; }}
			public int CompareTo(object o) { 
				if (Node == ((INode)o).Node) return 0;
				return Cost < ((INode)o).Cost ? -1 : 1; }
		}



		void Astar()
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();

			// Criar nova heuristica
			Heuristic heuristic = new Heuristic(end);

			LinqListManager<AstarNodeRecord> open   = new LinqListManager<AstarNodeRecord>();
			LinqListManager<AstarNodeRecord> closed = new LinqListManager<AstarNodeRecord>();

			AstarNodeRecord startRecord = new AstarNodeRecord();
			startRecord.node = start;
			startRecord.connection = null;
			startRecord.costSofar = 0f;
			startRecord.estimatedTotalCost = heuristic.GetEstimatedCost(start);

			open.Add(startRecord);

			// ---

			AstarNodeRecord current = null;
			while (open.Count > 0)
			{
//				Thread.Sleep(1);
				// procurar nodo com menor custo
				current = open.Min();
				// is this over yet?
				if (current.node == end) break;
				// obter conexoes
				List<Connection> connections = getConnections(current.node);

				// ---

				// foreach connection
				foreach (var connection in connections)
				{
					Point endNode = connection.to;
					// calcular custo acumulado desta conexao
					float endNodeCost = current.costSofar + connection.cost;

					bool wasFound = false;
					float endNodeHeuristic = 0; //// <-----
					// Ja processamos este nodo?
					AstarNodeRecord endNodeRecord = null;
					endNodeRecord = closed.Find(endNode);
					if (endNodeRecord != null)
					{
						// quando visitamos este nodo o custo era menor?
						if (endNodeRecord.costSofar < endNodeCost) continue;
						// nop, estamos num percurso mais curto. Reabrir nodo
						closed.Remove(endNodeRecord);
						endNodeHeuristic = endNodeRecord.estimatedTotalCost -
														endNodeRecord.costSofar;
					}

					// ---

					else if (
						(endNodeRecord = open.Find(endNode))
						     !=null) {
						// Encontrmaos na openList
						wasFound = true;
						if (endNodeRecord.costSofar <= endNodeCost)
							continue;
						endNodeHeuristic = endNodeRecord.estimatedTotalCost -
														endNodeRecord.costSofar;
					}

					// ----


					else
					{
						endNodeRecord = new AstarNodeRecord();
						endNodeRecord.node = endNode;
						endNodeHeuristic = heuristic.GetEstimatedCost(endNode);
					}
					endNodeRecord.costSofar = endNodeCost;
					endNodeRecord.connection = connection;
					endNodeRecord.estimatedTotalCost = endNodeCost +
															endNodeHeuristic;
					if (!wasFound) {
						markOpened(endNodeRecord.node);
						open.Add(endNodeRecord);
					}
				}
				open.Remove(current);
				closed.Add(current);
				markClosed(current.node);
			}

			/*if (current.node != end) return null;

			List<Connection> path = new List<Connection>();
			while (current.node != start)
			{
				path.Add(current.connection);
				current = closed.Find(r => r.node == current.connection.from);
			}
			path.Reverse();

			List<Vector2> points = path.Select<Connection, Vector2>(c => c.from).
								   ToList();
			points.Add(end);
			return points;
			*/

			sw.Stop();
			Console.WriteLine("A-Star took " + sw.Elapsed);
		}

		void DijkstraLinq ()
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();

			LinqListManager<NodeRecord> open = new LinqListManager<NodeRecord>();
			LinqListManager<NodeRecord> closed = new LinqListManager<NodeRecord>();
			
			NodeRecord startRecord = new NodeRecord();
			startRecord.node = start;
			startRecord.connection = null;
			startRecord.costSofar = 0;

			open.Add(startRecord);
			markOpened(startRecord.node);
			//

			NodeRecord current =null;
			
			while (open.Count > 0) {
//				Thread.Sleep(1);

				// procurar nodo com menor custo
				current = open.Min();
				// is this over yet?
				if (current.node == end) break;
				// obter conexoes
				List<Connection> connections = getConnections(current.node);

				// foreach connection
				foreach (var connection in connections)
				{
					Point endNode = connection.to;
					// Ja processamos este nodo?
					if (closed.Contains(endNode))
						continue;
					// calcular custo acumulado desta conexao
					int endNodeCost = current.costSofar + connection.cost;

					NodeRecord endNodeRecord = null;
					// Encontrmaos na openList?
					bool wasFound = false;
					endNodeRecord = open.Find(endNode);
					if (endNodeRecord != null)
					{
						// esta na lista
						wasFound = true;
						if (endNodeRecord.costSofar <= endNodeCost)
							continue;
					}
					else 
					{
						endNodeRecord = new NodeRecord();
						endNodeRecord.node = endNode;
					}
					endNodeRecord.costSofar = endNodeCost;
					endNodeRecord.connection = connection;
					if (!wasFound) {
						open.Add(endNodeRecord);
						markOpened(endNodeRecord.node);
					}
				}
				open.Remove(current);
				closed.Add(current);
				markClosed(current.node);
			}

			/*if (current.node != end) return null;

			List<Connection> path = new List<Connection>();
			while (current.node != start)
			{
				path.Add(current.connection);
				current = closed.Find(r => r.node == current.connection.from);
			}
			path.Reverse();

			List<Vector2> points = path.Select<Connection, Vector2>(c => c.from).
                                   ToList();
			points.Add(end);
			return points;*/

			sw.Stop();
			Console.WriteLine("Diksjtra Linq took " + sw.Elapsed);
		}

void DijkstraSorted()
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();

			SortedListManager<NodeRecord> open = new SortedListManager<NodeRecord>();
			SortedListManager<NodeRecord> closed = new SortedListManager<NodeRecord>();
			
			NodeRecord startRecord = new NodeRecord();
			startRecord.node = start;
			startRecord.connection = null;
			startRecord.costSofar = 0;

			open.Add(startRecord);
			markOpened(startRecord.node);
			//

			NodeRecord current =null;
			Console.WriteLine(open.Count);
			while (open.Count > 0) {
//				Thread.Sleep(1);

				// procurar nodo com menor custo
				current = open.Min();
				// is this over yet?
				if (current.node == end) break;
				// obter conexoes
				List<Connection> connections = getConnections(current.node);
				// foreach connection
				foreach (var connection in connections)
				{
					Point endNode = connection.to;
					// Ja processamos este nodo?
					if (closed.Contains(endNode))
						continue;
					// calcular custo acumulado desta conexao
					int endNodeCost = current.costSofar + connection.cost;

					NodeRecord endNodeRecord = null;
					// Encontrmaos na openList?
					bool wasFound = false;
					endNodeRecord = open.Find(endNode);
					
					if (endNodeRecord != null)
					{
						// esta na lista
						wasFound = true;
						if (endNodeRecord.costSofar <= endNodeCost)
							continue;
					}
					else 
					{
						endNodeRecord = new NodeRecord();
						endNodeRecord.node = endNode;
					}
					endNodeRecord.costSofar = endNodeCost;
					endNodeRecord.connection = connection;
					if (!wasFound) {
						open.Add(endNodeRecord);
						markOpened(endNodeRecord.node);
					}
				}
				open.Remove(current);
			//	System.Console.WriteLine("Adding to closed: " + current.Node);
				closed.Add(current);
				markClosed(current.node);
			}

			/*if (current.node != end) return null;

			List<Connection> path = new List<Connection>();
			while (current.node != start)
			{
				path.Add(current.connection);
				current = closed.Find(r => r.node == current.connection.from);
			}
			path.Reverse();

			List<Vector2> points = path.Select<Connection, Vector2>(c => c.from).
                                   ToList();
			points.Add(end);
			return points;*/

			sw.Stop();
			Console.WriteLine("Diksjtra Min took " + sw.Elapsed);
		}

		protected override void Update(GameTime gameTime)
		{
			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
				Exit();
	
			if (Keyboard.GetState().IsKeyDown(Keys.D) && tDikstra.Status == TaskStatus.Created) {
				int w = end.X + 1;
				int h = end.Y + 1;
				opened = new BitMatrix(h, w);
				closed = new BitMatrix(h, w);
				tDikstra.Start();
			}
			if (Keyboard.GetState().IsKeyDown(Keys.A) && tAstar.Status == TaskStatus.Created) {
				int w = end.X + 1;
				int h = end.Y + 1;
				opened = new BitMatrix(h, w);
				closed = new BitMatrix(h, w);
				tAstar.Start();
			}
			

			base.Update(gameTime);
		}

		protected override void Draw(GameTime gameTime)
		{

			graphics.GraphicsDevice.Clear(Color.CornflowerBlue);

			spriteBatch.Begin();
			for (int i = 0 ; i < maze.RowCount; i++) {
				for (int j = 0; j < maze.ColumnCount; j++) {
					if (maze[i,j]) {	
						spriteBatch.Draw(brick, new Vector2(j, i)*size, Color.Red );
					}
					else if (closed[i,j]) {
						spriteBatch.Draw(brick, new Vector2(j, i)*size, Color.Gray );
					} else if (opened[i,j]) {
						spriteBatch.Draw(brick, new Vector2(j, i)*size, Color.Green );
					} 
				}
			}
			spriteBatch.End();
			
			base.Draw(gameTime);
		}
	}
}








