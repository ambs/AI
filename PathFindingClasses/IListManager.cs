using System;
using Microsoft.Xna.Framework;

namespace PathFindingClasses
{
    interface IListManager<T> where T:INode
    {
       int Count { get; }
       void Add(T node);
       void Remove(T node);
       T Find(Point p);
       T Min();
       bool Contains(Point p);
    }
}