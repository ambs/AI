using Microsoft.Xna.Framework;
using System;
namespace PathFindingClasses
{
    interface INode : IComparable {
        Point Node {get;}
        float Cost {get;}
    }
}