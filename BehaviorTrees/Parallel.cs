﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace BehaviorTrees
{
	public class Parallel : INode
	{
		readonly IEnumerable<INode> childs;
		List<Thread> runningChilds;
		bool? result = null;

		public Parallel(IEnumerable<INode> children)
		{
			childs = children;
			runningChilds = new List<Thread>();
		}

		public bool Run()
		{
			result = null;
			foreach (var child in childs)
			{
				Thread t = new Thread(() => { runChild(child); });
				t.Start();
			}
			try
			{
				while (result.HasValue == false) Thread.Sleep(100);
			}
			catch (ThreadAbortException)
			{
				Terminate();	
			}

			return result.Value;
		}

		private void runChild(INode child)
		{
			runningChilds.Add(Thread.CurrentThread);
			bool returned = child.Run();
			runningChilds.Remove(Thread.CurrentThread);

			if (returned == false)
			{
				Terminate();
				result = false;
			}
			else if (runningChilds.Count == 0)
				result = true;
		}

		void Terminate()
		{
			foreach (var thread in runningChilds)
			{
				thread.Abort();
				thread.Join();
			}
		}

	}
}
