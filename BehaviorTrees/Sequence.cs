﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace BehaviorTrees
{
	public class Sequence : INode
	{
		readonly IEnumerable<INode> child;

		public Sequence(IEnumerable<INode> children)
		{
			child = children;
		}

		public bool Run()
		{
			foreach (INode c in child)
				if (!c.Run()) return false;
			return true;
		}


	}
}
