﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace BehaviorTrees
{
	public class Selector : INode
	{
		readonly IEnumerable<INode> child;

		public Selector(IEnumerable<INode> children)
		{
			child = children;
		}

		public bool Run()
		{
			foreach (INode c in child)
				if (c.Run()) return true;
			return false;
		}


	}
}
