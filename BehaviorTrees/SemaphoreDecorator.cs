﻿using System;
namespace BehaviorTrees
{
	public class SemaphoreDecorator : Decorator
	{
		readonly Semaphore semaphore;
		public SemaphoreDecorator(int limit, INode child) : base(child)
		{
			semaphore = new Semaphore(limit);
		}
		public SemaphoreDecorator(Semaphore semaphore, INode child) : base(child)
		{
			this.semaphore = semaphore;
		}

  		public override bool Run()
		{
			if (semaphore.Acquire())
			{
				var result = child.Run();
				semaphore.Release();
				return result;
			}
			else return false;
		}
	}
}
