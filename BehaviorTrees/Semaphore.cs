﻿using System;
namespace BehaviorTrees
{
	public class Semaphore
	{
		readonly int maximumUsers;
		int currentUsers;

		public Semaphore(int maxUsers)
		{
			currentUsers = 0;
			maximumUsers = maxUsers;
		}

		public bool Acquire()
		{
			if (currentUsers < maximumUsers)
			{
				currentUsers++;
				return true;
			}
			else
			{
				return false;
			}
		}

		public void Release()
		{
			currentUsers--;
		}
	}
}
