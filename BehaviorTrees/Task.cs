﻿using System;
using System.Threading;

namespace BehaviorTrees
{
	/// <summary>
	/// A Task is used both for conditionals or actions. They can be defined
	/// as a boolean function or a void one. In this case, success is returned.
	/// </summary>
	public class Task : INode
	{
		readonly Func<bool> action;
		readonly Func<bool> abortCall;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:BehaviorTrees.Task"/> class.
		/// This constructed is intended for use by subclasses.
		/// </summary>
		protected Task() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="T:BehaviorTrees.Task"/> class.
		/// </summary>
		/// <param name="action">The action or condition, as a function returning a boolean value.</param>
		public Task(Func<bool> action)
		{
			this.action = action;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:BehaviorTrees.Task"/> class.
		/// </summary>
		/// <param name="action">The action or condition, as a function returning a boolean value.</param>
		/// <param name="abort">Function to be called in case of abortion.</param>
		public Task(Func<bool> action, Func<bool> abort) {
			this.action = action;
			this.abortCall = abort;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:BehaviorTrees.Task"/> class.
		/// </summary>
		/// <param name="action">Action as a void function. True is returned by default.</param>
		public Task(Action action)
		{
			this.action = () => { action(); return true; };
		}

		/// <summary>
		/// Runs the defined action.
		/// </summary>
		public bool Run()
		{
			bool returnVal = false;
			try
			{
				returnVal = action();
			}
			catch (ThreadAbortException)
			{
				if (abortCall != null)
					returnVal = abortCall();
			}
			return returnVal;
		}


	}
}
