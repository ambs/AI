﻿using System;
namespace BehaviorTrees
{
	public class LimitDecorator : Decorator
	{
		readonly int runLimit = 0;
		int runSoFar = 0;

		public LimitDecorator(int count, INode c) : base(c) 
		{
			runLimit = count;
		}

		public override bool Run()
		{
			if (runSoFar < runLimit)
			{
				runSoFar++;
				return child.Run();
			}
			return false;
		}
	}
}
