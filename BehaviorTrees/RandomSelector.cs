﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace BehaviorTrees
{
	public class RandomSelector : INode
	{
		readonly IEnumerable<INode> child;
		readonly Random rng;

		public RandomSelector(IEnumerable<INode> children)
		{
			child = children;
			rng = new Random();
		}

		public bool Run()
		{
			
			foreach (INode c in child.OrderBy((x)=> rng.Next()))
				if (c.Run()) return true;
			return false;
		}


	}
}
