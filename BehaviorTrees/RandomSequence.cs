﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace BehaviorTrees
{
	public class RandomSequence : INode
	{
		readonly IEnumerable<INode> child;
		readonly Random rng;

		public RandomSequence(IEnumerable<INode> children)
		{
			child = children;
			rng = new Random();
		}

		public bool Run()
		{
			foreach (INode c in child.OrderBy((a)=>rng.Next()))
				if (!c.Run()) return false;
			return true;
		}

	
	}
}
