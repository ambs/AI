﻿using System;
using System.Threading;

namespace BehaviorTrees
{
	/// <summary>
	/// Decorator abstract class. Just declares the child var, and Run method.
	/// </summary>
	abstract public class Decorator : INode
	{
		protected INode child;


		protected Decorator(INode child)
		{
			this.child = child;
		}

		/// <summary>
		/// Runs decorator.
		/// </summary>
		public abstract bool Run();

	}
}
